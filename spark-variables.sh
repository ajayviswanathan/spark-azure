#!/usr/bin/env bash

#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# These variables are automatically filled in by the spark-ec2 script.
export MASTERS="146.148.46.1"
export SLAVES="23.251.148.157"
export HDFS_DATA_DIRS="/mnt/ephemeral-hdfs/data"
export MAPRED_LOCAL_DIRS="/mnt/hadoop/mrlocal"
export SPARK_LOCAL_DIRS="/mnt/spark"
export MODULES="spark"
export SPARK_VERSION="0.9.1"
export SHARK_VERSION="0.9.1"
export HADOOP_MAJOR_VERSION="1"
export SWAP_MB="{{swap}}"
export SPARK_WORKER_INSTANCES="1"
export SPARK_MASTER_OPTS="{{spark_master_opts}}"
